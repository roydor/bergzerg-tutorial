﻿using UnityEngine;
using System.Collections;
using System;

public class BuffItem : Item {
	private Hashtable buffs;

	public BuffItem() {
		buffs = new Hashtable();
	}

	public BuffItem(Hashtable ht)
	{
		buffs = ht;
	}

	public void AddBuff(BaseStat stat, int mod)
	{
		try
		{
			buffs.Add(stat.Name, mod);
		}
		catch(Exception e)
		{
			Debug.LogError(e.ToString());
		}
	}

	public int BuffCount()
	{
		return buffs.Count;
	}

	public Hashtable GetBuffs()
	{
		return buffs;
	}

	public void RemoveBuff(BaseStat stat, int mod)
	{
		buffs.Remove(stat.Name);
	}
}

﻿using UnityEngine;

public class Weapon : BuffItem 
{
	public int MaxDamage { get; set; }
	public float DamageVariance { get; set; }
	public float MaxRange { get; set; }
	public DamageType TypeOfDamage { get; set;}

	public Weapon()
	{
		MaxDamage = 0;
		DamageVariance = 0;
		MaxRange = 0;
		TypeOfDamage = DamageType.Bludgeon;
	}

	public Weapon(int maxDmg, float dmgVariance, float range, DamageType type)
	{
		MaxDamage = maxDmg;
		DamageVariance = dmgVariance;
		MaxRange = range;
		TypeOfDamage = type;
	}
}

public enum DamageType
{
	Bludgeon,
	Pierce,
	Slash,
	Fire,
	Ice,
	Lightning,
	Acid,
}

﻿using UnityEngine;
using System.Collections;

public class Item {
	public string Name { get; set; }
	public int Value { get; set;}
	public RarityTypes Rarity { get; set; }
	public int CurrentDurability { get; set; }
	public int MaxDurability { get; set; }

	public Item()
	{
		Name = "Need Name";
		Value = 0;
		Rarity = RarityTypes.Common;
		MaxDurability = 50;
		CurrentDurability = MaxDurability;
	}

	public Item(string name, int value, RarityTypes rarity, int maxDurabilitiy, int currentDurability)
	{
		Name = name;
		Value = Value;
		Rarity = rarity;
		MaxDurability = maxDurabilitiy;
		CurrentDurability = currentDurability;
	}
}

public enum RarityTypes {
	Common,
	Uncommon,
	Rare
}
﻿using UnityEngine;
using System.Collections;
using System;

public class GameSettings : MonoBehaviour {
	/// <summary>
	/// The name of the GameObject which the player will spawn at at the start of a level.
	/// </summary>
	public const string PLAYER_SPAWN_POINT = "Player Spawn Point";

	void Awake() {
		DontDestroyOnLoad(this);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SaveCharacterData()
	{
		GameObject pc = GameObject.Find("pc");
		PlayerCharacter pcClass = pc.GetComponent<PlayerCharacter>();

		PlayerPrefs.DeleteAll();
		PlayerPrefs.SetString("PlayerName", pcClass.Name);
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++)
		{
			PlayerPrefs.SetInt(((AttributeName)i).ToString() + ".BaseValue", pcClass.GetPrimaryAttribute(i).BaseValue);
			PlayerPrefs.SetInt(((AttributeName)i).ToString() + ".ExpToLevel", pcClass.GetPrimaryAttribute(i).ExpToLevel);
		}

		for(int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++)
		{
			PlayerPrefs.SetInt(((VitalName)i).ToString() + ".CurrentValue", pcClass.GetVital(i).CurrentValue);
			PlayerPrefs.SetInt(((VitalName)i).ToString() + ".BaseValue", pcClass.GetVital(i).BaseValue);
			PlayerPrefs.SetInt(((VitalName)i).ToString() + ".ExpToLevel", pcClass.GetVital(i).ExpToLevel);
		}

		for(int i = 0; i < Enum.GetValues(typeof(SkillName)).Length; i++)
		{
			PlayerPrefs.SetInt(((SkillName)i).ToString() + ".BaseValue", pcClass.GetSkill(i).BaseValue);
			PlayerPrefs.SetInt(((SkillName)i).ToString() + ".ExpToLevel", pcClass.GetSkill(i).ExpToLevel);
		} 
	}

	public void LoadCharacterData()
	{
		GameObject pc = GameObject.Find("pc");
		PlayerCharacter pcClass = pc.GetComponent<PlayerCharacter>();

		pcClass.Name = PlayerPrefs.GetString("PlayerName", "NameMe");
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++)
		{
			pcClass.GetPrimaryAttribute(i).BaseValue = PlayerPrefs.GetInt(((AttributeName)i).ToString() + ".BaseValue", 0);
			pcClass.GetPrimaryAttribute(i).ExpToLevel = PlayerPrefs.GetInt(((AttributeName)i).ToString() + ".ExpToLevel", 0);
		}

		for(int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++)
		{
			pcClass.GetVital(i).BaseValue = PlayerPrefs.GetInt(((VitalName)i).ToString() + ".BaseValue", 0);
			pcClass.GetVital(i).ExpToLevel = PlayerPrefs.GetInt(((VitalName)i).ToString() + ".ExpToLevel", 0);
		
			pcClass.GetVital(i).Update();

			pcClass.GetVital(i).CurrentValue = PlayerPrefs.GetInt(((VitalName)i).ToString() + ".CurrentValue", 0);
		}

		for(int i = 0; i < Enum.GetValues(typeof(SkillName)).Length; i++)
		{
			pcClass.GetSkill(i).BaseValue = PlayerPrefs.GetInt(((SkillName)i).ToString() + ".BaseValue", 0);
			pcClass.GetSkill(i).ExpToLevel = PlayerPrefs.GetInt(((SkillName)i).ToString() + ".ExpToLevel", 0);
			
			pcClass.GetSkill(i).Update();
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MobGenerator : MonoBehaviour {
	public enum State {
		Idle,
		Initialize,
		Setup,
		SpawnMob,
	}

	/// <summary>
	/// Local variable which holds the current state.
	/// </summary>
	public State state;

	/// <summary>
	/// All the diferent mobs we'll want to spawn
	/// </summary>
	public GameObject[] mobPrefabs;

	/// <summary>
	/// All the spawn points in the scene.
	/// </summary>
	public GameObject[] spawnPoints;

	void Awake()
	{
		state = State.Initialize;
	}

	// Use this for initialization
	IEnumerator Start () {
		while(true) {
			switch(state)
			{
			case State.Idle:
				break;
			case State.Initialize:
				Initialize();
				break;
			case State.Setup:
				Setup();
				break;
			case State.SpawnMob:
				SpawnMob();
				break;
			}
			yield return 0;
		}
	}

	private void Initialize()
	{
		if (!CheckForMobPrefabs())
		{
			Debug.LogError("No prefabs in spawn point");
			return;
		}

		if (!CheckForSpawnPoints())
		{
			Debug.LogError("No spawnpoints set in spawn point");
			return;
		}

		state = State.Setup;
	}
	
	private void Setup()
	{
		state = State.SpawnMob;
	}

	private void SpawnMob()
	{
		GameObject[] availableSpawnPoints = GetAvailableSpawnPoints();
		for(int i = 0; i < availableSpawnPoints.Length; i++)
		{
			GameObject newMob = Instantiate(mobPrefabs[Random.Range(0, mobPrefabs.Length)], availableSpawnPoints[i].transform.position, availableSpawnPoints[i].transform.rotation) as GameObject;
			newMob.transform.parent = availableSpawnPoints[i].transform;
		}
		state = State.Idle;
	}

	/// <summary>
	/// Generate a list of spawnpoints that do not have any mobs under it.
	/// </summary>
	private GameObject[] GetAvailableSpawnPoints()
	{
		List<GameObject> availableSpawnPoints = new List<GameObject>();
		for(int i = 0; i < spawnPoints.Length; i++)
		{
			if (spawnPoints[i].transform.childCount == 0)
			{
				availableSpawnPoints.Add(spawnPoints[i]);
			}
		}
		return availableSpawnPoints.ToArray();
	}

	/// <summary>
	/// CHeck to see that we have at least 1 mob prefab to spawn
	/// </summary>
	private bool CheckForMobPrefabs()
	{
		return mobPrefabs.Length > 0;
	}

	/// <summary>
	/// Check to see if we have spawn points set.
	/// </summary>
	private bool CheckForSpawnPoints()
	{
		return spawnPoints.Length > 0;
	}
}

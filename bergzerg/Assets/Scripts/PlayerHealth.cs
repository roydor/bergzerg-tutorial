﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

	public int MaxHealth = 100;
	public int CurrentHealth;

	public float HealthBarLength;

	// Use this for initialization
	void Start () {
		CurrentHealth = MaxHealth;
		HealthBarLength = Screen.width / 2;
	}
	
	// Update is called once per frame
	void Update () {
		AdjustCurrentHealth(0);
	}

	void OnGUI() {
		GUI.Box(new Rect(10,10, HealthBarLength, 20), CurrentHealth + "/" + MaxHealth);
	}

	public void AdjustCurrentHealth(int adj)
	{
		CurrentHealth += adj;
		CurrentHealth = Mathf.Clamp(CurrentHealth, 0, MaxHealth);

		HealthBarLength = (Screen.width / 2) * CurrentHealth / MaxHealth;
	}
}

﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

	public GameObject target;
	public float attackTimer;
	public float coolDown;

	// Use this for initialization
	void Start () {
		attackTimer = 0f;
		coolDown = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if (attackTimer > 0)
		{
			attackTimer -= Time.deltaTime;
		}
		if (attackTimer < 0)
		{
			attackTimer = 0;
		}

		if (Input.GetKeyUp(KeyCode.F) && attackTimer == 0) {
			Attack();
		}
	}

	private void Attack()
	{
		float distance = Vector3.Distance(target.transform.position, transform.position);

		Vector3 dir = target.transform.position - transform.position;
		float dot = Vector3.Dot(dir, transform.forward);
		Debug.Log(dot);

		if (distance < 2.5f && dot > 0)
		{
			EnemyHealth eh = (EnemyHealth) target.GetComponent<EnemyHealth>();
			eh.AdjustCurrentHealth(-10);
			attackTimer = coolDown;
		}
	}
}

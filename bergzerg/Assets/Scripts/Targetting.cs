﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script can be attached to any game object and is responsible for allowing the player to target different mobs
/// within range.
/// </summary>
public class Targetting : MonoBehaviour {

	public List<Transform> targets;
	public Transform selectedTarget;

	private Transform myTransform;


	// Use this for initialization
	void Start () {
		targets = new List<Transform>();
		selectedTarget = null;

		myTransform = transform;
		AddAllEnemies();
	}

	public void AddAllEnemies()
	{
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Enemy");

		foreach(GameObject enemy in gameObjects)
		{
			AddTarget(enemy.transform);
		}
	}

	public void AddTarget(Transform transform)
	{
		targets.Add(transform);
	}

	private void SortTargetsByDistance()
	{
		targets.Sort( delegate(Transform t1, Transform t2) { 
			return Vector3.Distance(t1.position, myTransform.position).CompareTo(Vector3.Distance(t2.position, myTransform.position));
		});
	}

	private void TargetEnemy()
	{
		if (targets.Count == 0)
		{
			AddAllEnemies();
		}

		if (targets.Count > 0)
		{
			if (selectedTarget == null)
			{
				SortTargetsByDistance();
				selectedTarget = targets[0];
			}
			else
			{
				int index = targets.IndexOf(selectedTarget);
				index = (index + 1) % targets.Count;
				DeselectTarget();
				selectedTarget = targets[index];
			}

			SelectTarget();
		}
	}

	private void DeselectTarget()
	{
		Transform mobName = selectedTarget.FindChild("Name");
		mobName.GetComponent<MeshRenderer>().enabled = false;
		//selectedTarget.renderer.material.color = Color.blue;
		selectedTarget = null;
		Messenger<bool>.Broadcast("show mob vitalbars", false);
	}

	private void SelectTarget()
	{
		Transform mobName = selectedTarget.FindChild("Name");

		if (mobName == null)
		{
			Debug.LogError("Cannot find the name mesh on " + selectedTarget.name);
		}

		mobName.GetComponent<TextMesh>().text = selectedTarget.GetComponent<Mob>().Name;
		mobName.GetComponent<MeshRenderer>().enabled = true;
		selectedTarget.GetComponent<Mob>().DisplayHealth();

		Messenger<bool>.Broadcast("show mob vitalbars", true);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			TargetEnemy();
		}
	}
}

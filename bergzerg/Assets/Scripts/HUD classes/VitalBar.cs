﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class is responsible for displaying one of the vitals of the player or a mob.
/// </summary>
public class VitalBar : MonoBehaviour {

	/// <summary>
	/// Represents whether this VitalBar belongs to the player or not.
	/// </summary>
	public bool IsPlayerHealthBar;

	/// <summary>
	/// How long the bar can be at 100%
	/// </summary>
	private int _maxBarLength;

	/// <summary>
	/// The current length of the vital bar.
	/// </summary>
	private int _currentBarLength;

	private GUITexture _display;

	void Awake()
	{
		_display = GetComponent<GUITexture>();
	}

	// Use this for initialization
	void Start () {
		_maxBarLength = (int)_display.pixelInset.width;
		OnEnable();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// This method is called when the GameObject is enabled.
	/// </summary>
	public void OnEnable()
	{
		if (IsPlayerHealthBar)
		{
			Messenger<int, int>.AddListener("Player Health Update", OnChangeHealthBarSize);
			Messenger<bool>.AddListener("show player vitalbars", ToggleDisplay);
		}
		else
		{
			Messenger<int, int>.AddListener("Mob Health Update", OnChangeHealthBarSize);
			Messenger<bool>.AddListener("show mob vitalbars", ToggleDisplay);
		}
	}

	/// <summary>
	/// This method is called when the GameObject is disabled.
	/// </summary>
	public void OnDisable()
	{
		if (IsPlayerHealthBar)
		{
			Messenger<int, int>.RemoveListener("Player Health Update", OnChangeHealthBarSize);
			Messenger<bool>.RemoveListener("show player vitalbars", ToggleDisplay);
		}
		else
		{
			ToggleDisplay(false);
			Messenger<int, int>.RemoveListener("Mob Health Update", OnChangeHealthBarSize);
			Messenger<bool>.RemoveListener("show mob vitalbars", ToggleDisplay);
		}
	}

	/// <summary>
	/// This method will calculate the total size of teh healthbar in relation to the % of the health the target has left.
	/// </summary>
	/// <param name="currentValue">Current value.</param>
	/// <param name="maxValue">Max value.</param>
	public void OnChangeHealthBarSize(int currentValue, int maxValue)
	{
		_currentBarLength = (int) (((float) currentValue / maxValue) * _maxBarLength);
		//_display.pixelInset = new Rect(_display.pixelInset.x, _display.pixelInset.y, _currentBarLength, _display.pixelInset.height);
		_display.pixelInset = CalculatePosition();
	}
	
	/// <summary>
	/// Setting the healthbar owner to the player or the mob.
	/// </summary>
	/// <param name="isPlayer">If set to <c>true</c> is player.</param>
	public void SetPlayerHealthBar(bool isPlayer)
	{
		IsPlayerHealthBar = isPlayer;
	}

	private Rect CalculatePosition()
	{
		float yPos = (_display.pixelInset.height / 2) - 10;

		if (!IsPlayerHealthBar)
		{
			float xPos = (_maxBarLength - _currentBarLength) - (_maxBarLength / 4 + 10);
			return new Rect(xPos, yPos, _currentBarLength, _display.pixelInset.height);
		}
		else
		{
			return new Rect(_display.pixelInset.x, yPos, _currentBarLength, _display.pixelInset.height);
		}
	}

	private void ToggleDisplay(bool show)
	{
		_display.enabled = show;
	}
}

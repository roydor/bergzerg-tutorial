﻿/// <summary>
/// This is the class for all of the character attributes in the game.
/// </summary>
using UnityEngine;


public class Attribute : BaseStat {
	/// <summary>
	/// Starting cost for all of the attributes.
	/// </summary>
	new public const int STARTING_EXP_COST = 50;

	public Attribute()
	{
		ExpToLevel = STARTING_EXP_COST;
		LevelModifier = 1.05f;
	}
}

/// <summary>
/// This is a list of all the attributes that we will have in-game for our characters.
/// </summary>
public enum AttributeName
{
	Might,
	Constitution,
	Nimbleness,
	Speed,
	Concentration,
	Willpower,
	Charisma,
};
﻿using System;
using UnityEngine;

/// <summary>
/// This class contains all the extra functions for a character's vitals.
/// </summary>
public class Vital : ModifiedStat {
	private int _currentValue;

	public Vital() {
		Debug.Log("Vital Created");
		_currentValue = 0;
		ExpToLevel = 40;
		LevelModifier = 1.1f;
	}

	/// <summary>
	/// The current value of this vital.
	/// Ensures that when you get the value it is a sane value.
	/// </summary>
	/// <value>The current value.</value>
	public int CurrentValue
	{
		get
		{ 
			// Make sure the value is not above the max.
			_currentValue = Mathf.Clamp(_currentValue, 0, AdjustedBaseValue);
			return _currentValue; 
		}
		set { _currentValue = value; }
	}
}

/// <summary>
/// This enum is just a list of the vitals our character will have.
/// </summary>
public enum VitalName
{
	Health,
	Energy,
	Mana,
}

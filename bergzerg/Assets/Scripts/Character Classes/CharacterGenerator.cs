﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterGenerator : MonoBehaviour {

	private const int MIN_ATTRIBUTE_VALUE = 10;
	private const int STARTING_ATTRIBUTE_VALUE = 50;
	private const int STARTING_POINTS = 350;

	private const int OFFSET = 5;
	private const int LINE_HEIGHT = 20;
	private const int STAT_LABEL_WIDTH = 100;
	private const int BASEVALUE_LABEL_WIDTH = 50;
	private const int BUTTON_WIDTH = 20;
	private const int BUTTON_HEIGHT = 20;
	private const int STAT_STARTING_POS = 40;

	private const int CREATE_BUTTON_WIDTH = 100;

	private PlayerCharacter _toon;
	private int _pointsLeft;

	public GameObject playerPrefab;

	// Use this for initialization
	void Start () {
		GameObject pc = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		pc.name = "pc";
		_toon = pc.GetComponent<PlayerCharacter>();

		_pointsLeft = STARTING_POINTS;
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++)
		{
			_toon.GetPrimaryAttribute(i).BaseValue = STARTING_ATTRIBUTE_VALUE;
			_pointsLeft -= STARTING_ATTRIBUTE_VALUE - MIN_ATTRIBUTE_VALUE;
		}
		_toon.StatUpdate();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI() 
	{
		DisplayName();
		DisplayPointsLeft();
		DisplayAttributes();
		DisplayVitals();
		DisplaySkills();

		if (_pointsLeft > 0 || _toon.Name == String.Empty)
		{
			DisplayCreateButtonLabel();
		}
		else
		{
			DisplayCreateButton();
		}
	}

	private void DisplayCreateButtonLabel()
	{
		GUI.Label(
			new Rect(Screen.width / 2 - CREATE_BUTTON_WIDTH / 2, 
		         STAT_STARTING_POS + (LINE_HEIGHT * 10), 
		         CREATE_BUTTON_WIDTH, 
		         LINE_HEIGHT),
			"Creating", 
			"Button");
	}

	private void DisplayCreateButton()
	{
		if (GUI.Button(
			new Rect(Screen.width / 2 - CREATE_BUTTON_WIDTH / 2, 
		         STAT_STARTING_POS + (LINE_HEIGHT * 10), 
		         CREATE_BUTTON_WIDTH, 
		         LINE_HEIGHT),
			"Create"))
		{
			GameObject gs = GameObject.Find("__GameSettings");
			GameSettings gsScript = gs.GetComponent<GameSettings>();
			
			for(int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++)
			{
				_toon.GetVital(i).CurrentValue = _toon.GetVital(i).AdjustedBaseValue;
			}
			
			gsScript.SaveCharacterData();
			Application.LoadLevel("Level1");
		}
	}

	private void DisplayPointsLeft()
	{
		GUI.Label(new Rect(250, 10, 100, 25),"Points Left: " + _pointsLeft);
	}

	private void DisplayName() 
	{
		GUI.Label(
			new Rect(10, 
		         10, 
		         50, 
		         25), 
			"Name:");
		_toon.Name = GUI.TextField(
			new Rect(65, 
		         10, 
		         100, 
		         25), 
			_toon.Name);
	}

	private void DisplayAttributes()
	{
		for(int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++)
		{
			GUI.Label(
				new Rect(OFFSET, 
			         STAT_STARTING_POS + (LINE_HEIGHT * i), 
			         STAT_LABEL_WIDTH, 
			         LINE_HEIGHT), 
				((AttributeName)i).ToString());

			GUI.Label(
				new Rect(OFFSET + STAT_LABEL_WIDTH, 
			         STAT_STARTING_POS + (LINE_HEIGHT * i), 
			         BASEVALUE_LABEL_WIDTH, 
			         LINE_HEIGHT), 
				_toon.GetPrimaryAttribute(i).AdjustedBaseValue.ToString());

			if (GUI.Button(new Rect(OFFSET + STAT_LABEL_WIDTH + BASEVALUE_LABEL_WIDTH, 40 + (LINE_HEIGHT * i), BUTTON_WIDTH, BUTTON_HEIGHT), "-"))
			{
				if (_toon.GetPrimaryAttribute(i).BaseValue > MIN_ATTRIBUTE_VALUE)
				{
					_toon.GetPrimaryAttribute(i).BaseValue--;
					_pointsLeft++;
					_toon.StatUpdate();
				}
			}

			if (GUI.Button(
				new Rect(OFFSET + STAT_LABEL_WIDTH + BASEVALUE_LABEL_WIDTH + BUTTON_WIDTH, 
			         40 + (LINE_HEIGHT * i), 
			         BUTTON_WIDTH, 
			         BUTTON_HEIGHT),
				"+"))
			{
				if (_pointsLeft > 0)
				{
					_toon.GetPrimaryAttribute(i).BaseValue++;
					_pointsLeft--;
					_toon.StatUpdate();
				}
			}
		}
	}

	private void DisplayVitals()
	{
		for(int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++)
		{
			GUI.Label(
				new Rect(
					OFFSET, 
					STAT_STARTING_POS + (LINE_HEIGHT * (i + 7)), 
					STAT_LABEL_WIDTH, 
					LINE_HEIGHT), 
				((VitalName)i).ToString());

			GUI.Label(
				new Rect(
					OFFSET + STAT_LABEL_WIDTH, 
					STAT_STARTING_POS + (LINE_HEIGHT * (i + 7)), 
					BASEVALUE_LABEL_WIDTH, 
					LINE_HEIGHT), 
				_toon.GetVital(i).AdjustedBaseValue.ToString());
		}
	}

	private void DisplaySkills()
	{
		for(int i = 0; i < Enum.GetValues(typeof(SkillName)).Length; i++)
		{
			GUI.Label(
				new Rect(
					OFFSET + STAT_LABEL_WIDTH + BASEVALUE_LABEL_WIDTH + BUTTON_WIDTH + BUTTON_WIDTH + OFFSET * 2, 
					STAT_STARTING_POS + (LINE_HEIGHT * i), 
					STAT_LABEL_WIDTH, 
					LINE_HEIGHT), 
				((SkillName)i).ToString());

			GUI.Label(
				new Rect(
					OFFSET + STAT_LABEL_WIDTH + BASEVALUE_LABEL_WIDTH + BUTTON_WIDTH + BUTTON_WIDTH + OFFSET * 2 + STAT_LABEL_WIDTH, 
					STAT_STARTING_POS + (LINE_HEIGHT * i), 
					BASEVALUE_LABEL_WIDTH, 
					LINE_HEIGHT), 
				_toon.GetSkill(i).AdjustedBaseValue.ToString());
		}
	}
}

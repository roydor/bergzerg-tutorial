﻿using System.Collections.Generic;

/// <summary>
/// Base class for all stats that will be modified by attributes
/// </summary>
using UnityEngine;


public class ModifiedStat : BaseStat {
	/// <summary>
	/// A list of attributes that modify this stat.
	/// </summary>
	private List<ModifyingAttribute> _mods;

	/// <summary>
	/// The amount added to the baseValue from the modifiers.
	/// </summary>
	private int _modValue;

	/// <summary>
	/// Initializes a new instance of the <see cref="ModifiedStat"/> class.
	/// </summary>
	public ModifiedStat()
	{
		_mods = new List<ModifyingAttribute>();
		_modValue = 0;
	}

	/// <summary>
	/// Add a ModifyingAttribute to our list of mods for this stat.
	/// </summary>
	/// <param name="mod">Mod.</param>
	public void AddModifier(ModifyingAttribute mod)
	{
		_mods.Add(mod);
	}

	/// <summary>
	/// Reset _modValue to 0.
	/// Iterate through the list of modifying attributes and add the adjusted base value multiplied by ratio
	/// to our _modValue.
	/// </summary>
	private void CalculateModValue()
	{
		_modValue = 0;

		foreach(ModifyingAttribute att in _mods)
		{
			_modValue += (int)(att.attribute.AdjustedBaseValue * att.ratio);
		}
	}

	/// <summary>
	/// This is overriding the AdjustedBaseValue from BaseStat.
	/// Calculates teh AdjustedBaseValue from teh BaseValue + BuffValue + _modValue.
	/// </summary>
	/// <value>The adjusted base value.</value>
	public new int AdjustedBaseValue 
	{
		get { return BaseValue + BuffValue + _modValue; }
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	public void Update() 
	{
		CalculateModValue();
	}
}

/// <summary >
/// A structure which will hold an attribute and a ratio which can be added as a modifying attribute to a ModifiedStat.
/// </summary>
public struct ModifyingAttribute
{
	public Attribute attribute;
	public float ratio;

	public ModifyingAttribute(Attribute attr, float rat)
	{
		attribute = attr;
		ratio = rat;
	}
}
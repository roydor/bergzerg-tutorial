﻿/// <summary>
/// This class contains all the extra functions that are needed for a skill.
/// </summary>
using UnityEngine;


public class Skill : ModifiedStat {
	/// <summary>
	/// A bool to toggle whether a character knows a skill.
	/// </summary>
	public bool IsKnown { get; set;}

	public Skill()
	{
		Debug.Log("Skill created");
		IsKnown = false;
		ExpToLevel = 25;
		LevelModifier = 1.1f;
	}
}

/// <summary>
/// An enumeration list for all the skills in the game.
/// </summary>
public enum SkillName {
	Melee_Offence,
	Melee_Defence,
	Ranged_Offence,
	Ranged_Defence,
	Magic_Offence,
	Magic_Defence,
}
﻿using UnityEngine;
using System.Collections;
using System;

public class BaseCharacter : MonoBehaviour {

	public string Name { get; set; }
	public int Level { get; set; }
	public uint FreeExp { get; set; }

	private Attribute[] _primaryAttributes;
	private Vital[] _vitals;
	private Skill[] _skills;

	public void Awake()
	{
		Name = String.Empty;
		Level = 0;
		FreeExp = 0;

		_primaryAttributes = new Attribute[Enum.GetValues(typeof(AttributeName)).Length];
		_vitals = new Vital[Enum.GetValues(typeof(VitalName)).Length];
		_skills = new Skill[Enum.GetValues(typeof(SkillName)).Length];

		SetupPrimaryAttributes();
		SetupVitals();
		SetupSkills();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddExp(uint exp)
	{
		FreeExp += exp;
		CalculateLevel();
	}

	/// <summary>
	/// Take the avg of all the player's skills and assign that as the player level.
	/// </summary>
	public void CalculateLevel()
	{

	}

	private void SetupPrimaryAttributes()
	{
		for(int i = 0; i < _primaryAttributes.Length; i++)
		{
			_primaryAttributes[i] = new Attribute();
			_primaryAttributes[i].Name = ((AttributeName)i).ToString();
		}
	}

	private void SetupVitals()
	{
		for(int i = 0; i < _vitals.Length; i++)
		{
			_vitals[i] = new Vital();
		}

		SetupVitalModifiers();
	}

	private void SetupSkills()
	{
		for(int i = 0; i < _skills.Length; i++)
		{
			_skills[i] = new Skill();
		}

		SetupSkillModifiers();
	}

	public Attribute GetPrimaryAttribute(int index) {
		return _primaryAttributes[index];
	}

	public Vital GetVital(int index) {
		return _vitals[index];
	}

	public Skill GetSkill(int index) {
		return _skills[index];
	}

	private void SetupVitalModifiers()
	{
		// health
		GetVital((int)VitalName.Health).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Constitution), 0.5f));

		// energy
		GetVital((int)VitalName.Energy).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Constitution), 1.0f));

		// mana
		GetVital((int)VitalName.Mana).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Willpower), 1.0f));
	}

	public void SetupSkillModifiers()
	{
		// melee offense
		GetSkill((int)SkillName.Melee_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Might), 0.33f));
		GetSkill((int)SkillName.Melee_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Nimbleness), 0.33f));

		// melee def
		GetSkill((int)SkillName.Melee_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed), 0.33f));
		GetSkill((int)SkillName.Melee_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Constitution), 0.33f));

		// magic off
		GetSkill((int)SkillName.Magic_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration), 0.33f));
		GetSkill((int)SkillName.Magic_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Willpower), 0.33f));

		// magic def
		GetSkill((int)SkillName.Magic_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration), 0.33f));
		GetSkill((int)SkillName.Magic_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Willpower), 0.33f));
	
		// ranged off
		GetSkill((int)SkillName.Ranged_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration), 0.33f));
		GetSkill((int)SkillName.Ranged_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed), 0.33f));

		// ranged def
		GetSkill((int)SkillName.Ranged_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed), 0.33f));
		GetSkill((int)SkillName.Ranged_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Nimbleness), 0.33f));	
	}

	public void StatUpdate()
	{
		for(int i = 0; i < _vitals.Length; i++)
		{
			_vitals[i].Update();
		}

		for(int i = 0; i < _skills.Length; i++)
		{
			_skills[i].Update();
		}
	}
}

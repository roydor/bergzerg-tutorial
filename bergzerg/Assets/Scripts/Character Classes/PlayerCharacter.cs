﻿using UnityEngine;
using System.Collections;

public class PlayerCharacter : BaseCharacter {

	void Update() 
	{
		Messenger<int, int>.Broadcast("Player Health Update", 80, 100);
	}
}

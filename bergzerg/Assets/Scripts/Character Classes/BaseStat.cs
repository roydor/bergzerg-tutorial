﻿using UnityEngine;

/// <summary>
/// This is the base class for all stats in the game.
/// </summary>
public class BaseStat 
{
	/// <summary>
	/// Publically accessable value for all base stats to start at.
	/// </summary>
	public const int STARTING_EXP = 100;

	/// <summary>
	/// The base value of the stat.
	/// </summary>
	public int BaseValue { get; set;}

	/// <summary>
	/// The amount that this stat is buffed.
	/// </summary>
	public int BuffValue { get; set;}

	/// <summary>
	/// The total amount of xp needed to raise this skill.
	/// </summary>
	public int ExpToLevel { get; set;}

	/// <summary>
	/// The modifier applied to the xp needed to raise the skill.
	/// </summary>
	public float LevelModifier { get; set;}

	public string Name { get; set; }
	
	/// <summary>
	/// Initializes a new instance of the <see cref="BaseStat"/> class.
	/// </summary>
	public BaseStat()
	{
		BaseValue = 0;
		BuffValue = 0;
		LevelModifier = 1.1f;
		ExpToLevel = STARTING_EXP;
		Name = "";
	}

	/// <summary>
	/// Calculates the exp to level.
	/// </summary>
	/// <returns>The exp to level.</returns>
	private int CalculateExpToLevel()
	{
		return (int)(ExpToLevel * LevelModifier);
	}

	/// <summary>
	/// Assign the new value to _expTolevel and increase the base value by 1.
	/// </summary>
	public void LevelUp()
	{
		ExpToLevel = CalculateExpToLevel();
		BaseValue++;
	}

	/// <summary>
	/// Recalculate the AdjustedBaseValue and return it.
	/// </summary>
	/// <value>The adjusted base value.</value>
	public int AdjustedBaseValue
	{
		get { return BaseValue + BuffValue; }
	}
}
﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	public Transform target;
	public float moveSpeed;
	public float rotationSpeed;
	public int maxDistance;

	private Transform _myTransform;

	void Awake() {
		_myTransform = transform;
		maxDistance = 2;
	}

	// Use this for initialization
	void Start () {
		GameObject gameObject = GameObject.FindGameObjectWithTag("Player");
		target = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawLine(_myTransform.position, target.position, Color.yellow);

		// Look at the target
		_myTransform.rotation = Quaternion.Slerp(
			_myTransform.rotation, 
			Quaternion.LookRotation(target.position - _myTransform.position),
			rotationSpeed * Time.deltaTime);

		if (Vector3.Distance(target.position, _myTransform.position) > maxDistance)
		{
			_myTransform.position += _myTransform.forward * moveSpeed * Time.deltaTime;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {
	
	public GameObject playerCharacterPrefab;
	public GameObject gameSettingsPrefab;

	public Camera mainCamera;

	public float zOffset;
	public float yOffset;
	public float xRotOffset;
	
	private GameObject _pc;
	private PlayerCharacter _pcScript;

	/// <summary>
	/// The place in 3d space where the player will spawn.
	/// </summary>
	private Vector3 _playerSpawnPointPos;

	// Use this for initialization
	void Start () {
		// Default position for player spawn point.
		_playerSpawnPointPos = new Vector3(35, 2, 87);

		GameObject spawnPoint = GameObject.Find(GameSettings.PLAYER_SPAWN_POINT);
		if (spawnPoint == null)
		{
			Debug.LogWarning("Can not find spawn point:" + GameSettings.PLAYER_SPAWN_POINT);
			spawnPoint = new GameObject(GameSettings.PLAYER_SPAWN_POINT);

			Debug.Log ("Moved Player Spawn point.");
			spawnPoint.transform.position = _playerSpawnPointPos;

			Debug.Log("Created Player Spawnpoint:" + GameSettings.PLAYER_SPAWN_POINT);
		}
		_pc = Instantiate(playerCharacterPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
		_pc.name = "pc";
		_pcScript = _pc.GetComponent<PlayerCharacter>();

		zOffset = -2.5f;
		yOffset = 2.5f;
		xRotOffset = 25.0f;

		mainCamera.transform.position = new Vector3(_pc.transform.position.x, _pc.transform.position.y + yOffset, _pc.transform.position.z + zOffset);
		mainCamera.transform.Rotate(xRotOffset, 0, 0);

		LoadCharacter();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void LoadCharacter() {
		GameObject gs = GameObject.Find("__GameSettings");
		if (gs == null)
		{
			gs = Instantiate(gameSettingsPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			gs.name = "__GameSettings";
		}

		GameSettings gsScript = gs.GetComponent<GameSettings>();
		gsScript.LoadCharacterData();
	}
}
